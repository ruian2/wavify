//
// Created by Frankshammer42 on 12/6/17.
//

#include <time.h>
#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>
#include "ftt_utility.h"
using namespace cv;
using namespace std;
using namespace Eigen;


int main(int argc, char** argv){
    char* imageName = argv[1];
    clock_t t_start = clock();
    Eigen::MatrixXd* z_value = generate_z_value(imageName, 100);
    std::cout << "Spent " << double(clock() - t_start)/CLOCKS_PER_SEC << " seconds" << std::endl;
    int rows = z_value->rows();
    int cols = z_value->cols();
    std::cout << rows * cols << std::endl;
    Eigen::MatrixXd result = z_value->array()/(rows*cols);
    cv::Mat to_display;
    cv::eigen2cv(result, to_display);
    std::cout<< to_display << std::endl;
    cv::normalize(to_display, to_display, 1, 0, NORM_MINMAX);
    cv::Mat display_result;
    to_display.convertTo(display_result, CV_32F);
    cv::imshow("result", display_result);
    cv::waitKey(0);
    // TODO: Need to make threading generating process autonomous
//    std::cout << result << std::endl;
    delete(z_value);
}
