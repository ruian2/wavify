//
// Created by Frankshammer42 on 12/6/17.
//

#ifndef PROJECT_WAVIFY_FTT_UTILITY_H
#define PROJECT_WAVIFY_FTT_UTILITY_H

#include <Eigen/Core>
#include <Eigen/Dense>
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>
using namespace cv;
using namespace std;
using namespace Eigen;

Eigen::MatrixXd* meshgrid_hb(int x_size, int y_size, int which_one);

Eigen::MatrixXd* synthesis(double x_freq, double y_freq, double complex_a, double complex_b,
                int row_size, int col_size, Eigen::MatrixXd* X_Data, Eigen::MatrixXd* Y_Data);

float calcEntropy(cv::Mat& img);

cv::Mat create_grid_flip(cv::Mat& img);

std::vector<cv::Mat> dft(cv::Mat& img);

Eigen::MatrixXd* generate_z_value(char* image_name, int max_wave_number);

#endif //PROJECT_WAVIFY_FTT_UTILITY_H
