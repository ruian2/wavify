/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name : fft_utility.cpp
* Creation Date : 03-09-2017
* Created By : Rui An  
_._._._._._._._._._._._._._._._._._._._._.*/

#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>
#include <complex.h>
#include <math.h>

using namespace cv;
using namespace std;
using namespace Eigen;

/*
 * utility function to generate data
 */

Eigen::MatrixXd* meshgrid_hb(int x_size, int y_size, int which_one){
    if(which_one == 0){
        Eigen::VectorXd x_value = Eigen::VectorXd::LinSpaced(x_size,0,x_size-1);
        Eigen::MatrixXd* X_data = new Eigen::MatrixXd(y_size, x_size);



        //Eigen::MatrixXd X_data = Eigen::MatrixXd(y_size, x_size);
//        cout << x_value << endl;
        for(int i=0; i<y_size; i++){
            (*X_data).row(i) = x_value;
        }

        //cout << *X_data << endl;
        return X_data;
    }
    else if(which_one == 1){
        Eigen::VectorXd y_value = Eigen::VectorXd::LinSpaced(y_size,0,y_size-1);
        Eigen::MatrixXd* Y_data = new Eigen::MatrixXd(y_size, x_size);
        for(int i=0; i<x_size; i++){
            (*Y_data).col(i) = y_value;
        }
        //cout << *Y_data << endl;
        return Y_data;
    }
    return NULL;
}


/*
 * Return the height result for data modeling
*/
Eigen::MatrixXd* synthesis(double x_freq, double y_freq, double complex_a, double complex_b,
                           int row_size, int col_size, Eigen::MatrixXd* X_Data, Eigen::MatrixXd* Y_Data){
//    std::cout << x_freq << std::endl;
//    std::cout << y_freq << std::endl;
//    std::cout << '\n' << std::endl;
//    Eigen::MatrixXd* X_Data = meshgrid_hb(row_size, col_size, 0);
//    Eigen::MatrixXd* Y_Data = meshgrid_hb(row_size, col_size, 1);
//    std::cout << X_Data->block(2,2,2,2) << std::endl;
//    std::cout << Y_Data->block(452,452,2,2) << std::endl;
    Eigen::MatrixXd* Z_Data = new Eigen::MatrixXd(col_size, row_size);
    Eigen::MatrixXd inside_x = X_Data->array() * x_freq;
    Eigen::MatrixXd inside_y = Y_Data->array() * y_freq;
//    std::cout << inside_x.block(634,634,2,2) << std::endl;
    Eigen::MatrixXd cos_x = Eigen::cos(inside_x.array());
    Eigen::MatrixXd cos_y = Eigen::cos(inside_y.array());
    Eigen::MatrixXd sin_x = Eigen::sin(inside_x.array());
    Eigen::MatrixXd sin_y = Eigen::sin(inside_y.array());
//    std::cout << inside_x.block(452,452,2,2) << std::endl;
//    std::cout << cos_x.block(452,452,2,2) << std::endl;
//    std::cout << complex_a << std::endl;
//    std::cout << complex_b << std::endl;
//    std::cout << "\n" << std::endl;
    *Z_Data = cos_x.cwiseProduct(cos_y)*complex_a + sin_x.cwiseProduct(sin_y)*complex_b;
//    std::cout << Z_Data->row(col_size-1) << std::endl;
//    delete(X_Data);
//    delete(Y_Data);
    return Z_Data;
}


//TODO: Need to normalize entropy value
float calcEntropy(cv::Mat& img){
    cv::Mat gray_image = img;
	float range[] = { 0, 256 } ;
    const float* histRange = { range };
	int histSize = 256; bool uniform = true; bool accumulate = false;
	cv::Mat hist;
    calcHist(&gray_image, 1, 0, Mat(), hist, 1, &histSize, &histRange, uniform, accumulate);
	auto hist_sum_list = sum(hist);
	float hist_sum = hist_sum_list[0]; 
	cv::Mat logP;
    cv::log(hist, logP);
    double entropy = -1*sum(hist.mul(logP)).val[0];
	std::cout << entropy << std::endl;
	return entropy;
} 


cv::Mat create_grid_flip(cv::Mat& img){
    int rows = img.size().height;
	int cols = img.size().width;
	cv::Mat container_image = cv::Mat::zeros(2*rows, 2*cols, CV_32F);
	img.copyTo(container_image(cv::Rect(0,0,cols,rows)));
	cv::Mat img_left;
	cv::flip(container_image, img_left, 0);

	cv::Mat img_right;
	cv::flip(img_left, img_right, 1);

	cv::Mat lower_half;
	cv::bitwise_or(img_left, img_right, lower_half);

	cv::Mat upper_half;
	cv::flip(lower_half,upper_half,0);

	cv::Mat result_image;
	cv::bitwise_or(lower_half, upper_half, result_image);

	return result_image;
}


// We padded the original to make it more efficient to
// calculate
std::vector<cv::Mat> dft(cv::Mat& img){
    cv::Mat padded;
	int m = cv::getOptimalDFTSize(img.rows);
	int n = cv::getOptimalDFTSize(img.cols);
	cv::copyMakeBorder(img, padded, 0, m-img.rows, 0, n-img.cols, BORDER_CONSTANT, cv::Scalar::all(0));
	//RE part and IM part of the number
	cv::Mat complex_number_planes[] = {cv::Mat_<float>(padded), cv::Mat::zeros(padded.size(), CV_32F)};
	cv::Mat complex_number;
	cv::merge(complex_number_planes, 2, complex_number);
	cv::dft(complex_number, complex_number);

//    cv::Mat invDFT, invDFTcvt;
//    idft(complex_number, invDFT, DFT_SCALE | DFT_REAL_OUTPUT ); // Applying IDFT
//    invDFT.convertTo(invDFTcvt, CV_8U);
//    cv::imshow("Output", invDFTcvt);
//    cv::waitKey(0);


    cv::split(complex_number, complex_number_planes);
    std::vector<cv::Mat> complex_result = {complex_number_planes[0], complex_number_planes[1]};
	return complex_result;
}

/*
 * Generate z values for the waves simulation for openframework
 */
Eigen::MatrixXd* generate_z_value(char* image_name, int max_wave_number){
    cv::Mat img = cv::imread(image_name, CV_LOAD_IMAGE_GRAYSCALE);
    cv::Mat grid_result = create_grid_flip(img);

    std::vector<cv::Mat> complex_number_planes = dft(grid_result);
    int x = complex_number_planes[0].rows;
    int y = complex_number_planes[1].cols;
    Eigen::MatrixXd* Z_value = new Eigen::MatrixXd(y, x);
    Eigen::MatrixXd* X_Data = meshgrid_hb(x, y, 0);
//    std::cout << *X_Data << std::endl;
    Eigen::MatrixXd* Y_Data = meshgrid_hb(x, y, 1);
//    std::cout << *Y_Data << std::endl;
    for (int i = 0; i < max_wave_number; ++i) {
        // Use the 20 20 block at the top-left corner of the frequency space
        int i_index = (i/20);
        int j_index = (i%20);

//        cout << "why the fuck" << endl;
//        cout << i_index/double(x) << endl;
        double x_freq = 2*M_PI*(i_index/double(x));
        double y_freq = 2*M_PI*(j_index/double(y));

        float complex_a = 0.0;
        float complex_b = 0.0;

//        std::cout << "Real Number and Imaginary Number are" << std::endl;
//        std::cout << complex_number_planes[0].at<float>(i_index, j_index) << std::endl;
//        std::cout << complex_number_planes[1].at<float>(i_index, j_index) << std::endl;
//        std::cout << '\n' << std::endl;

        if ((i_index == 0 && j_index == 0) || (i_index == x/2-1 && j_index == y/2-1)){
            complex_a = complex_number_planes[0].at<float>(i_index, j_index)/float(x);
            complex_b = complex_number_planes[1].at<float>(i_index, j_index)/float(y/2);
        }
        else{
            complex_a = complex_number_planes[0].at<float>(i_index, j_index)/float(x/2);
            complex_b = complex_number_planes[1].at<float>(i_index, j_index)/float(y/2);
        }
//        clock_t t_start = clock();
        Eigen::MatrixXd* temp_result = synthesis(x_freq, y_freq, complex_a, complex_b, x, y, X_Data, Y_Data);
//        std::cout << "Spent " << double(clock() - t_start)/CLOCKS_PER_SEC << " seconds" << std::endl;
        Z_value->array()  = Z_value->array() + temp_result->array();
        delete(temp_result);
    }
    Eigen::MatrixXd* z_value_result = new Eigen::MatrixXd(y/2, x/2);
    (*z_value_result) = Z_value->block(0,0,y/2,x/2);
    delete(Z_value);
    return z_value_result;
}







