//
//  Particle.cpp
//  wavify
//
//  Created by Frankshammer42 on 12/8/17.
//
#pragma onece

#include "Particle.hpp"
#include "ofMain.h"

//Hard Coded 300

Particle::Particle(int index){
    particle_index = index;
    trail_progress_indicator = 0;
}

void Particle::set_progress(int progress){
    trail_progress_indicator = progress;
}

void Particle::set_position(ofVec3f z){
    pos = z;
}

void Particle::set_current_trail_z(float z){
    this->current_trail_z = z;
}

void Particle::update(){
    float liquidness = 5;
    float amplitude = 20.0;
    float speedDampen = 5;
    float x = pos.x;
    float y = pos.y;
    float z = pos.z;
    z += ofSignedNoise(x/liquidness, y/liquidness, z/liquidness, ofGetElapsedTimef()/speedDampen)*amplitude;
    if(particle_index == 0){
        std::cout << trail_progress_indicator << std::endl;
    }
    if(particle_index == 0 && trail_progress_indicator  == 1999){
        std::cout << "Finished Compositing" << std::endl;
        ofVec3f newPos(x,y,z);
        set_position(newPos);
        return;
    }


    float previous_trail_z = trail[trail_progress_indicator];
    int next_trail_mark = trail_progress_indicator;
    if(trail_progress_indicator < 1999){
        ++next_trail_mark;
    }
    float next_trail_z = trail[next_trail_mark];

//    Still in the progress of going from one trail point to another
    if ((current_trail_z <= next_trail_z && current_trail_z >= previous_trail_z) ||
        (current_trail_z >= next_trail_z && current_trail_z <= previous_trail_z)){

        if(current_trail_z == next_trail_z){
            trail_progress_indicator = next_trail_mark;
        }
        else{
            float diff = next_trail_z - previous_trail_z;
            float delta_z = diff/2;
            float current_z = current_trail_z;
            float new_z = current_z + delta_z;
            set_current_trail_z(new_z);
        }
    }
    else{
        set_current_trail_z(next_trail_z);
        trail_progress_indicator = next_trail_mark;
    }
    
    float scaled_z_value = ofMap(current_trail_z, min_value, max_value, -2000, 2000);
    ofVec3f newPos(x,y,scaled_z_value);
    set_position(newPos);
}

void Particle::set_trail(std::vector<float> particle_trail){
    trail = particle_trail;
}


