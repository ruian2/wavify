#pragma once

#include "ofMain.h"
#include "Particle.hpp"
using namespace std;

class ofApp : public ofBaseApp {
	public:
		void setup();
		void update();
		void draw();
		
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y);
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    
        void assign_trails();
		ofEasyCam cam;
		ofMesh mesh;
		ofImage img;
        std::vector<Particle> particles_group;
        std::vector<std::vector<float>> all_the_waves;
        void read_wave_into_particles(int wave_number);
        void read_in_all_wave();
    
    std::vector<float> read_coords(int wave_number);
    std::vector<std::vector<float>> read_data();
};
