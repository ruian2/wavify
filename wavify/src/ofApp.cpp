#include "ofApp.h"
#include <algorithm>
#include <ctime>

using namespace std;
//--------------------------------------------------------------
void ofApp::setup() {
	ofSetVerticalSync(true);
    ofSetFrameRate(100);
    ofEnableDepthTest();
    cam.setFarClip(-1);
    ofVec3f myCamGlobalPosition = cam.getGlobalPosition();
    ofQuaternion myCamRotation = cam.getGlobalOrientation();
    
    ofQuaternion new_orientation;
    new_orientation.makeRotate(90, 1, 0, 0);
    ofVec3f newGlobalPosition = ofVec3f(0,0,300);
    cam.setOrientation(new_orientation);
    cam.setGlobalPosition(newGlobalPosition);
    
    int skip = 1; // load a subset of the points

    for(int x = 0; x < 600; x += skip){
        for(int y=0; y < 300; y += skip){
            int current_index = x*300 + y;
            Particle particle_toadd(current_index);
            int world_x = x;
            int world_y = y;
            ofVec3f pos(30*x, 30*y, 5);
            particle_toadd.set_position(pos);
            particles_group.push_back(particle_toadd);
        }
    }
    cout << particles_group.size() << endl;
    read_in_all_wave();
    std::cout << "Read all into the particles" << std::endl;

    
    
    //We use the final wave to specify the max and min coords
    std::vector<float> fft_coords = read_coords(1999);
    auto min_max_coords = std::minmax_element(fft_coords.begin(), fft_coords.end());
    float min_coord = *min_max_coords.first;
    float max_coord = *min_max_coords.second;
    std::cout << "Min value is " << min_coord << std::endl;
    std::cout << "Max value is " << max_coord << std::endl;
	mesh.setMode(OF_PRIMITIVE_POINTS);
    //Initialize the particles to its initial position
    for(int x = 0; x < 600; x += skip){
        for(int y=0; y < 300; y += skip){
            int current_index = x*300 + y;
            int world_x = x - 300;
            int world_y = y - 150;
            float z_value = particles_group[current_index].trail[0];
            float scaled_z_value = ofMap(z_value, min_coord, max_coord, -2000, 2000);
            ofVec3f pos(30*world_x, 30*world_y, scaled_z_value);
            particles_group[current_index].current_trail_z = z_value;

            particles_group[current_index].min_value = min_coord;
            particles_group[current_index].max_value = max_coord;

            particles_group[current_index].set_position(pos);
            mesh.addVertex(pos);
        }
    }


    //Initialize the particles to its initial position
//    for(int x = 0; x < 300; x += skip){
//        for(int y=0; y < 300; y += skip){
//            int current_index = x*300 + y;
//            int world_x = x - 150;
//            int world_y = y - 150;
//            float z_value = fft_coords[current_index];
//            float scaled_z_value = ofMap(z_value, min_coord, max_coord, -2000, 2000);
//            ofVec3f pos(30*world_x, 30*world_y, scaled_z_value);
//            Particle particle_toadd(current_index);
//            particle_toadd.current_trail_z = z_value;
//
//            particle_toadd.min_value = min_coord;
//            particle_toadd.max_value = max_coord;
//
//            particle_toadd.set_position(pos);
//            particles_group.push_back(particle_toadd);
//            mesh.addVertex(pos);
//        }
//    }

    
//    std::cout << "Group Size is " << particles_group.size() << std::endl;
//    std::cout << cam.getGlobalPosition() << std::endl;
//    std::cout << cam.getGlobalOrientation() << std::endl;

	glEnable(GL_POINT_SMOOTH); // use circular points instead of square points
}

//--------------------------------------------------------------
void ofApp::update() {
//    ofVec3f myCamGlobalPosition = cam.getGlobalPosition();
//    ofQuaternion myCamRotation = cam.getGlobalOrientation();
//    std::cout << myCamGlobalPosition << std::endl;
//    std::cout << myCamRotation << std::endl;
//    for(Particle particle : particles_group){
//        particle.update();
//        mesh.setVertex(particle.particle_index, particle.pos);
//    }
    
    for (std::vector<Particle>::iterator it = particles_group.begin(); it != particles_group.end(); ++it)
    {
        Particle& particle = *it;
        particle.update();
        mesh.setVertex(particle.particle_index, particle.pos);
        // do stuff here
    }
//    for (int i = 0; i < particles_group.size(); i++){
//        particles_group[i].update();
//        mesh.setVertex(particles_group[i].particle_index, particles_group[i].pos);
//    }
    
}

//--------------------------------------------------------------
void ofApp::draw() {
    ofSetBackgroundColor(0,0,0);
	cam.begin();
	mesh.draw();
	cam.end();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

//-------------------------------------------------------------
void ofApp::read_wave_into_particles(int wave_number){
    std::string str_index = std::to_string(wave_number);
    std::string file_name = "result_" + str_index + ".txt";
    ofBuffer buffer = ofBufferFromFile(file_name);
    std::cout << file_name << std::endl;
    int rows = 0;
    int cols = 0;
    for (auto line : buffer.getLines()){
        std::vector<std::string> whole_row = ofSplitString(line, " ");

        cols = 0;
        for (std::string i : whole_row){
//            std::cout << rows << std::endl;
//            std::cout << cols << std::endl;
            int particle_index = rows*300  + cols;
            float temp = ::atof(i.c_str());
            particles_group[particle_index].trail.push_back(temp);
            cols ++;
        }
        rows += 1;
    }
    //    std::cout << coords.size() << std::endl;
}

void ofApp::read_in_all_wave(){
    for(int i = 0; i < 2000; i++){
        clock_t begin = clock();
        read_wave_into_particles(i);
        clock_t end = clock();
        double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
        std::cout << "Spend " << elapsed_secs << " reading data" << std::endl;
    }
    std::cout << "Finish reading data" << std::endl;
}

//TODO: Now just assume it is 300 300
//Read a particular wave to the data
std::vector<float> ofApp::read_coords(int wave_index){
	std::vector<float> coords;
    std::string str_index = std::to_string(wave_index);
    std::string file_name = "result_" + str_index + ".txt";
    ofBuffer buffer = ofBufferFromFile(file_name);
    int rows = 0;
    int cols = 0;
    for (auto line : buffer.getLines()){
        std::vector<std::string> whole_row = ofSplitString(line, " ");
        cols = whole_row.size();
        for (std::string i : whole_row){
            float temp = ::atof(i.c_str());
            coords.push_back(temp);
        }
        rows += 1;
    }
    coords.pop_back();
//    std::cout << coords.size() << std::endl;
    return coords;
}

//Read all relavent data to the vectors
std::vector<std::vector<float>> ofApp::read_data(){
    std::vector<std::vector<float>> result;
    for(int i = 0; i < 2000; i++){
        result.push_back(read_coords(i));
    }
    return result;
    std::cout << "Finish reading data" << std::endl;
}

//assign trail data to the particles
void ofApp::assign_trails(){
    for(Particle particle : particles_group){
        std::cout << particle.particle_index << std::endl;
        std::vector<float> particle_trail;
        for(std::vector<float> wave : all_the_waves){
            particle_trail.push_back(wave[particle.particle_index]);
        }
        particle.set_trail(particle_trail);
    }
}




