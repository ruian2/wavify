//
//  Particle.hpp
//  wavify
//
//  Created by Frankshammer42 on 12/8/17.
//

#ifndef Particle_hpp
#define Particle_hpp

#include <stdio.h>
#include "ofMain.h"
class Particle{
    public:
        Particle(int index);
        void set_position(ofVec3f z);
        void update();
        void set_trail(std::vector<float> trail);
        void set_current_trail_z(float z);
        void set_progress(int progress);
        float min_value;
        float max_value;
        //current particle position
        ofVec3f pos;
        //current float position in the data
        float current_trail_z;
        //index for get the mesh index
        int particle_index;
        //indicate which trail point it last been through
        //will be used for animation
        int trail_progress_indicator;
        std::vector<float> trail;
};

#endif /* Particle_hpp */
