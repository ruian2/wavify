#-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
 #File Name : demo_wave_single_image.py
 #Creation Date : 03-12-2017
 #Created By : Rui An  
#_._._._._._._._._._._._._._._._._._._._._.


import numpy as np
from numpy import pi as pi
import cv2
import time
from mpl_toolkits.mplot3d import Axes3D 
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.collections import PolyCollection


def calcEntropy(img):
    hist = cv2.calcHist([img],[0],None,[256],[0,256])
    hist = hist.ravel()/hist.sum()
    logs = np.log2(hist+0.00001)
    entropy = -1 * (hist*logs).sum()
    return entropy  


def get_entropy_list(video_name):
    camera = cv2.VideoCapture(video_name)
    entropy_list = []
    while True:
        grabbed, frame = camera.read()
        if grabbed:
            img = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            entropy_list.append(calcEntropy(img)) 
            # print harris_result
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            break
    camera.release()
    return entropy_list

#We need to use this method inorder to generate the right answer;
def create_grid_flip(img):
    row_size, col_size = img.shape
    grid = np.zeros((2*row_size, 2*col_size))
    grid[0:row_size, 0:col_size] = img
    img_hori = cv2.flip(grid, 0)
    img_left = cv2.bitwise_or(img_hori, grid)
    img_right = cv2.flip(img_left, 1)
    result_image = cv2.bitwise_or(img_left, img_right)
    return result_image
    # cv2.imwrite("canvas_testing.jpg", result_image)



def create_grid_copy(img):
    row_size, col_size = img.shape
    grid = np.zeros((2*row_size, 2*col_size))
    grid[0:row_size, 0:col_size] = img
    grid[0:row_size, col_size:(2*col_size)] = img
    grid[row_size:2*row_size, 0:col_size] = img
    grid[row_size:(2*row_size), col_size:(2*col_size)] = img
    return grid
    # cv2.imwrite("canvas_testing.jpg", grid)
    

def fft(img):
    # img = cv2.imread('./canvas_testing.jpg', 0)
    # img = cv2.imread('./canvas_testing.jpg', 0)
    # cv2.imshow("Original", img)
    # cv2.waitKey(0)
    x,y = img.shape
    #This dft has all the values we need for the recovery of the photo, I hope
    dft = cv2.dft(np.float32(img),flags = cv2.DFT_COMPLEX_OUTPUT)
    # dft = np.fft.fftshift(dft) # I actually don't want to have it shift
    dft_shift = np.fft.fftshift(dft)
    result_magnititude = 20*np.log(cv2.magnitude(dft[:,:,0],dft[:,:,1]))
    result_magnititude = np.uint8(result_magnititude)
    result_color_magnititude = cv2.cvtColor(result_magnititude, cv2.COLOR_GRAY2RGB)
    cv2.imwrite("wc_frequency_magnitude.jpg", result_color_magnititude)
    return dft, result_magnititude


def synthesis(x_freq, y_freq, complex_a, complex_b, row_size, col_size, complex_dft_result, phase):
    X_data = np.arange(row_size)
    Y_data = np.arange(col_size)
    X_data, Y_data = np.meshgrid(X_data, Y_data)
    '''
    This line can make a symmetry of the input images
    '''
    Z_data = complex_a*(np.cos(x_freq*X_data))*(np.cos(y_freq*Y_data)) + \
            complex_b*(np.sin(x_freq*X_data))*(np.sin(y_freq*Y_data))
    rows,cols = Z_data.shape
    # print Z_data[rows-1]
    return Z_data


def get_single_image_result(img_name):
    img = cv2.imread(img_name)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    grid_result = create_grid_copy(img)
    complex_dft_result, result_magnititude = fft(grid_result)
    x,y,_ = complex_dft_result.shape
    print x
    print y
    print x*y
    X_value = np.arange(x)
    Y_value = np.arange(y)
    Z_value = np.zeros((y,x))
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    for i in range(2000):
        file_name = "result_" + str(i) + ".txt"
        print file_name
        # print "Now in the", i, "frame"
        i_index = int(i)/20
        j_index = int(i)%20
        x_freq = 2*np.pi*(i_index/float(x))
        progress = 100*(float(i_index*x)/float(x*y))
        # if result_magnititude[i_index][j_index] < 20:
            # continue
        y_freq = 2*np.pi*(j_index/float(y))

        # print complex_dft_result[i_index][j_index][0]
        # print complex_dft_result[i_index][j_index][1]
        # print "\n"
        if ((i_index==0) and (j_index==0)) or ((i_index==((x/2)-1)) and ((j_index==((y/2)-1)))):
            complex_a = complex_dft_result[i_index][j_index][0]/(x)
            complex_b = -complex_dft_result[i_index][j_index][1]/(y/2)
        else:
            complex_a = complex_dft_result[i_index][j_index][0]/(x/2)
            complex_b = -complex_dft_result[i_index][j_index][1]/(y/2)
        phase = np.arctan2(complex_b,complex_a)

        # start_time = time.time()
        temp_result = synthesis(x_freq, y_freq, complex_a, complex_b, x, y, complex_dft_result, phase)
        # print "Spent", time.time()-start_time, "on this"
        Z_row, Z_col = Z_value.shape
        print Z_row
        print Z_col
        Z_value = Z_value + temp_result
        Z_value_to_save = Z_value/float(x*y)
        np.set_printoptions(suppress=True)
        np.savetxt(file_name, Z_value_to_save[0:(Z_row/2),0:(Z_col/2)], '%.08f')


    to_show = Z_value/float(x*y)
    X_value, Y_value = np.meshgrid(X_value, Y_value)
    X_row, X_col = X_value.shape
    Y_row, Y_col = Y_value.shape
    Z_row, Z_col = Z_value.shape
    surf = ax.plot_surface(X_value[0:(X_row/2),0:(X_col/2)], Y_value[0:(Y_row/2),0:(Y_col/2)], to_show[0:(Z_row/2),0:(Z_col/2)], cmap=cm.coolwarm,linewidth=0, antialiased=False)
    azim_value = int(0)
    elev_value = int(0)
    ax.view_init(azim=azim_value, elev=elev_value)
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    fig.colorbar(surf, shrink=1.0, aspect=5)
    plt.show()

    # surf = ax.plot_wireframe(X_value[0:(X_row/2),0:(X_col/2)], Y_value[0:(Y_row/2),0:(Y_col/2)], to_show[0:(Z_row/2),0:(Z_col/2)],antialiased=False)

    return to_show 
    
            
    
start_time = time.time()
to_show = get_single_image_result("../src_pic/jiangshan.jpg")
print to_show
print to_show.size
# np.set_printoptions(suppress=True)
# np.savetxt('result.txt', to_show, '%.08f')



# print z_value


